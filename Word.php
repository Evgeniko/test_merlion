<?php
/**
 * Created by PhpStorm.
 * User: AIIST
 * Date: 29.11.2018
 * Time: 23:00
 */

class Word
{
    public $source; //начальное слово
    public $need; //слово которое нужно получить
    public $dictionary; //словарь
    private $abc; //алфавит
    public $trace = []; //история сгенерируемых слов
    private $bads = []; //массив ненужных слов

    //Информация для отладки
    private $count = 0;

    public function __construct($source, $need)
    {
        $dictionaryArray = require_once ("./dictionary.php");
        $this->dictionary = $dictionaryArray['words']; //Устанавливаем словарь слов
        $this->abc = $dictionaryArray['abc']; //Устанавливаем буквенный алфавит
        //Получаем данные с формы
        $this->source = mb_strtolower(trim($source));
        $this->need = mb_strtolower(trim($need));

        $this->trace[] = $this->source;
        //Запускаем метод перебора букв
        $this->mutation();
    }


    /**
     * @param int $char позиция элемента в строке
     * @return bool
     */
    public function mutation($char = 0)
    {
        $this->count++; //Просто счётчик

        $source = $this->source;
        $need = $this->need;

        //Остановить скрипт если ничего не находится
        if ($this->count > 25000){
            $this->view(["<h3>Ошибка вычисления</h3>"]);
            exit();
        }
        
        //Если прошли всё слово посимвольно
        if ($char > 4){
            //Получаем слова с перебором по алфавиту
            $newWord = $this->replaceCharOnAbc($this->source);
            if ($newWord){
                $this->source = $newWord; //Обновляем исходное слово
                $this->trace[] = $newWord;
                $this->mutation(0); //Перезапускаем перебор на нужные буквы

            }else{//Если не получили  слово
                //Проверяем есть ли предыдущие слова в истории
                if (count($this->trace) > 1){
                    //Добавляем текущее в ненужное и возвращаемся к предыдущему этапу
                    $this->bads[] = $this->source;
                    $this->source = array_pop($this->trace);
                    $this->mutation(0);
                }else{
                    //Если предыдущих слов нет, то начинаем с самого начала
                    $this->source = $this->trace[0];
                    $this->mutation(0);
                }
            }
        }

        //Проверка на полное совпадение слов
        if ($this->checkSame($source, $need)){
            //Нашли нужное слово
            $this->view($this->trace);
            exit();
        } else {
            //Пробуем заменить символ в строке на нужный
            $currentChar = mb_substr($source, $char, 1);
            $needChar = mb_substr($need, $char, 1);
            if($this->checkSame($currentChar, $needChar)){//Если нужный символ уже стоит, то переходим к следующему
                $this->mutation(++$char);
            }
            //Заменяем символ на нужный
            $newSource = str_replace($currentChar, $needChar, $source);
            if ($this->checkValid($newSource)){
                //Если получилось валидное слово, обновляем текущее слово и запускаем процесс заного
                $this->source = $newSource;
                $this->trace[] = $newSource;
                $this->mutation(0);
            }else{
                //Иначе переходим к следующему символу
                $this->mutation(++$char);
            }
        }
    }

    /**
     * Метод подстановки букв алфавита до первого валидного вхождения
     *
     * @param string $word
     * @return bool|mixed
     */
    private function replaceCharOnAbc(string $word)
    {
        //Меняем букву на любую из алфавита
        $length = mb_strlen($word);
        for ($i=0;$i<$length;$i++){
            foreach ($this->abc as $letter){
                $currentChar = mb_substr($word, $i, 1);
                $newWord = str_replace($currentChar, $letter, $word);
                if ($this->checkValid($newWord)){
                    return $newWord;
                }
            }
        }
        return false;
    }





    /**
     * Общая проверка сгенерированного слова на валидность
     *
     * @param $word
     * @return bool
     */
    private function checkValid($word):bool
    {
        if ($this->checkInDictionary($word) && !$this->checkOnBeforeWordInTrace($word) && !$this->checkInBads($word) && !$this->checkOnCurrentValue($word) && !$this->checkInTrace($word)){
            return true;
        }
        return false;
    }

    /**
     * Проверка слова на наличие в словаре
     *
     * @param string $word
     * @return bool
     */
    private function checkInDictionary(string $word):bool
    {
        return in_array($word, $this->dictionary);
    }

    /**
     * Проверка на совпадение строк или символов
     *
     * @param string $a
     * @param string $b
     * @return bool
     */
    private function checkSame(string $a, string $b):bool
    {
        return $a == $b;
    }

    /**
     * Проверка на наличие в массиве "ненужных" слов
     *
     * @param string $word
     * @return bool
     */
    private function checkInBads(string $word):bool
    {
        return in_array($word, $this->bads);
    }

    /**
     * Проверяем, было ли слово на предыдущем шаге
     *
     * @param string $word
     * @return bool
     */
    private function checkOnBeforeWordInTrace(string $word):bool
    {
        $beforeIndex = count($this->trace) - 2;
        if ($beforeIndex == -1){
            $beforeIndex = 0;
        }
        return $word == $this->trace[$beforeIndex];
    }

    /**
     * Проверка сгенерированного слова с текущим словом
     *
     * @param string $word
     * @return bool
     */
    private function checkOnCurrentValue(string $word):bool
    {
        return $word == $this->source;
    }

    /**
     * Проверка на нахождении в истории
     *
     * @param $word
     * @return bool
     */
    private function checkInTrace($word)
    {
        return in_array($word, $this->trace);
    }

    /**
     * Простенький вывод даннх пользователю
     *
     * @param $array
     */
    private function view($array)
    {
        $i = 0;
        foreach ($array as $item){
            $i++;
            if ($i == count($array)){
                echo "$item";
            }else{
                echo "$item->";
            }
        }

        $dictionaryHTML = "<ul>";
        foreach ($this->dictionary as $word){
            $dictionaryHTML.="<li>$word</li>";
        }
        $dictionaryHTML.= "</ul>";
        echo "<h5>Используемый словарь</h5>";
        echo $dictionaryHTML;
        die();
    }

}